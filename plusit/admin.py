from django.contrib import admin
from plusit.models import PlusitTemplate


class PlusitTemplateAdmin(admin.ModelAdmin):
    class Meta(object):
        model = PlusitTemplate

    list_display = ("template_name", "template_description", "name", "description", "period_type", "calculation", "vote", "language", )
    

admin.site.register(PlusitTemplate, PlusitTemplateAdmin)
