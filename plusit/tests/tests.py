import json

from datetime import datetime
from plusit.models import PlusitHistorical

from django.test import TestCase
from django.utils.timezone import utc


class ModelsTest(TestCase):
    fixtures = ['test_data.json']

    def test_calculate_avg_history(self):
        """
        Tests if history is calculated
        """
        plusit = PlusitHistorical.objects.all()[1]
        res = plusit.calculate_history(calc_type='avg')
        expected_value = {
        'axis_legend': '["2012-12-28T11:52:55.594000+00:00", "2012-12-28T11:53:05.785000+00:00", '
                       '"2012-12-28T11:53:14.452000+00:00", "2012-12-28T11:53:28.775000+00:00", '
                       '"2012-12-28T11:54:32.036000+00:00", "2012-12-28T11:54:52.929000+00:00"]',
        'history': '[[[0, 1.0], [1, 2.0], [2, 1.0], [3, 4.0], [4, 234.0], [5, 121.0]]]'}
        self.assertEqual(str(res), expected_value)
        res = plusit.calculate_history(calc_type='sum')
        expected_sums = {
        'axis_legend': '["2012-12-28T11:52:55.594000+00:00", "2012-12-28T11:53:05.785000+00:00", '
                       '"2012-12-28T11:53:14.452000+00:00", "2012-12-28T11:53:28.775000+00:00", '
                       '"2012-12-28T11:54:32.036000+00:00", "2012-12-28T11:54:52.929000+00:00"]',
        'history': '[[[0, 1.0], [1, 2.0], [2, 1.0], [3, 4.0], [4, 234.0], [5, 121.0]]]'}
        self.assertEqual(str(res), expected_sums)
