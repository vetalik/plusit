from decimal import Decimal

from django import forms
from django.forms import ValidationError

from plusit.models import (PlusitHistorical, HistoricValue,
                           PlusitTemplate, PERIOD_CHOICES, CALCULATION_CHOICES, LANGUAGES)


class PlusitHistoricalForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(PlusitHistoricalForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['name', 'description', 'period_type',
                                'calculation', 'public']
    class Meta:
        model = PlusitHistorical
        exclude = ('plusit_type', 'user')
    
    description = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}), required=False,)
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), required=True,)
    period_type = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), required=True, choices=PERIOD_CHOICES)
    calculation = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}), required=True, choices=CALCULATION_CHOICES)
    
    def clean_name(self):
        name = self.cleaned_data["name"]
        name_is_used = PlusitHistorical.objects.filter(name=name, user=self.request.user).exists()
        if name_is_used:
            raise ValidationError("This name is already in use.")
        return name


class HistoricValueForm(forms.Form):
    value = forms.CharField()
    plusit_id = forms.CharField()
    value_date = forms.DateTimeInput()
    comment = forms.CharField(required=False)

    def clean_value(self):
        value = self.cleaned_data["value"]
        value = Decimal(value.replace(',', '.').replace(" ", ""))
        return value

    def save(self, date):
        plusit = PlusitHistorical.objects.get(pk=int(self.cleaned_data['plusit_id']))
        data={'value_date': date,
              'value': Decimal(self.cleaned_data['value']),
              'plusit': plusit,
              'comment': self.cleaned_data['comment'],
              }
        obje = HistoricValue.objects.create(**data)


class PlusitTemplateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PlusitTemplateForm, self).__init__(*args, **kwargs)

    class Meta:
        model = PlusitTemplate
        exclude = ['vote', ]
    
    def clean_template_name(self):
        template_name = self.cleaned_data["template_name"]
        name_is_used = PlusitTemplate.objects.filter(template_name=template_name).exists()
        if name_is_used:
            raise ValidationError("This name is already in use.")
        return template_name

    template_description = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control', 'required':'required' }), required=True,)
    template_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'required':'required', "minlength":2,}), required=True,)
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'required':'required', "aria-invalid":"false"}), required=True,)
    description = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), required=False,)
    period_type = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control', 'required':'required'}), required=True, choices=PERIOD_CHOICES)
    calculation = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control', 'required':'required'}), required=True, choices=CALCULATION_CHOICES)
    language = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control', 'required':'required'}), required=True, choices=LANGUAGES)
