# -*- coding: utf-8 -*-
import pytz
import json

from datetime import datetime
from dateutil import parser

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.shortcuts import (render_to_response, render,
                              get_object_or_404, redirect)
from django.template import RequestContext
from django.contrib.auth import logout
from django.core.urlresolvers import reverse

from plusit.models import PlusitHistorical, PlusitTemplate, HistoricValue, PERIOD_CHOICES_DICT, CALC_TYPES, TemplateVotes, VOTE_CHOICES_DICT
from plusit.forms import (HistoricValueForm, PlusitHistoricalForm,
                          PlusitTemplateForm)
from plusit.filters import PlusitTemplateFilter, LANGUAGES

from social_auth.models import UserSocialAuth


@login_required
def logout_me(request):
    logout(request)
    return redirect('plusit_home')


def plusit_home(request):
    """
    Generates list of all user plusits
    """
    if not request.user.is_authenticated():
        return render_to_response(
            'home.html',
            context_instance=RequestContext(request))
    else:
        return redirect('plusits')

@login_required
def plusits(request, new_user=False):
    """
    Generates list of all user plusits
    """
    user = request.user
    historical_plusits = PlusitHistorical.objects.only_normal().filter(user=user)
    plusits = []

    return render_to_response(
        'plusits.html', {'plusits': historical_plusits,
                         'current_view': 'plusits',
                         'new_user': new_user, 
                         'empty_range': range(5-len(historical_plusits)),},
        context_instance=RequestContext(request))

@login_required
def plusits_sq(request, new_user=False):
    """
    Generates list of all user plusits
    """
    user = request.user
    historical_plusits = PlusitHistorical.objects.only_normal().filter(user=user)
    plusits = []

    return render_to_response(
        'plusits_square.html', {'plusits': historical_plusits,
                         'current_view': 'plusits',
                         'new_user': new_user, 
                         'empty_range': range(5-len(historical_plusits)),},
        context_instance=RequestContext(request))


@login_required
def plusit(request, plusit_id):
    """
    Plusit with chart and value input form
    """
    plusit = PlusitHistorical.objects.only_normal().get(pk=plusit_id)
    stat_type=request.GET.get('stat_type')
    period_type=int(request.GET.get('period_type', 0))
    if request.user!=plusit.user:
        raise Http404
        
    plusit_data = plusit.calculate_history(calc_type=stat_type, period=period_type)
    plusit_data['plusit'] = plusit
    form = HistoricValueForm()
    plusit_data['form'] = form
    plusit_data['current_view'] = 'plusit'
    plusit_data['stat_type'] = stat_type or plusit.calculation
    plusit_data['stat_types'] = CALC_TYPES
    plusit_data['period_type'] = period_type or plusit.period_type
    plusit_data['periods_choices'] = PERIOD_CHOICES_DICT
    return render(request, 'plusit_view.html', plusit_data)


@login_required
def stat_type_dafault(request, plusit_id, stat_type):
    if request.method == 'POST' and request.is_ajax():
        plusit = get_object_or_404(PlusitHistorical, pk=plusit_id, user=request.user)
        plusit.calculation=stat_type
        plusit.save()
        return HttpResponse('ok')
    else:
        return Http404()


@login_required
def period_type_default(request, plusit_id, period_type):
    if request.method == 'POST' and request.is_ajax():
        plusit = get_object_or_404(PlusitHistorical, pk=plusit_id, user=request.user)
        plusit.period_type=period_type
        plusit.save()
        return HttpResponse('ok')
    else:
        return Http404()
    

@login_required
def plusit_create(request):
    if request.method == 'GET':
        template_id = request.GET.get('template_id')
        if template_id:
            template = get_object_or_404(PlusitTemplate, pk=template_id)
            form = PlusitHistoricalForm(template.as_dict(), **{'request':request})
        else:
            form = PlusitHistoricalForm(**{'request':request})
        return render(request, 'plusit_create.html',
                      {'form': form,
                       'current_view': 'plusit_create'})
    elif request.method == 'POST':
        form = PlusitHistoricalForm(request.POST, **{'request':request})
        if form.is_valid():
            plusit = form.save(commit=False)
            plusit.user = request.user
            plusit.save()
            increment = 1
            count = PlusitHistorical.objects.only_normal().filter(user=request.user).count()
            if not count % 5:
                increment = 0
            page=count//5 + increment
            return redirect("{0}?page={1}".format(reverse('plusits'), page))

        return render(request, 'plusit_create.html',
                      {'form': form,
                       'current_view': 'plusit_create'})


@login_required
def plusit_value(request, plusit_id):
    history = None
    plusit_id = int(plusit_id)
    if request.method == 'POST':
        plusit = get_object_or_404(PlusitHistorical, pk=plusit_id, user=request.user)
        value_form = HistoricValueForm(request.POST)
        if value_form.is_valid():
            current_time = datetime.utcnow().replace(tzinfo=pytz.utc)
            date = parser.parse(request.POST.get('value_date'), dayfirst=True) if request.POST.get(
                'value_date') else current_time
            value_form.save(date)
            return redirect('plusit', **{'plusit_id': plusit_id})
        else:
            plusit_data = plusit.calculate_history()
            plusit_data['plusit'] = plusit
            plusit_data['form'] = value_form
            plusit_data['current_view'] = 'plusit'
            plusit_data['stat_type'] = plusit.calculation
            plusit_data['stat_types'] = CALC_TYPES
            plusit_data['period_type'] = plusit.period_type
            plusit_data['periods_choices'] = PERIOD_CHOICES_DICT
            plusit_data['history'] = history if history else None,
            return render(request, 'plusit_view.html',
                          plusit_data)
    else:
        plusit = get_object_or_404(PlusitHistorical, pk=plusit_id, user=request.user)
        history = HistoricValue.objects.filter(plusit=plusit)
    return render(request, 'plusit_view.html',
                  {'plusit': plusit,
                  'history': history if history else None,
                  'current_view': 'plusit_value'
                  }
                 )


def templates(request):
    if request.method == 'GET':
        templates = PlusitTemplate.objects.filter(vote__gt=settings.TEMPLATE_VOTES_TO_HIDE).order_by('-vote')
        f = PlusitTemplateFilter(request.GET, queryset=templates)
        votes = TemplateVotes.objects.filter(user=request.user).values('template__pk', 'vote')
        current_lang = request.GET.get('language', '')
        if not current_lang:
            current_lang = 'rus'
        
        for template in f:
            for vote in votes:
                if template.pk == vote['template__pk']:
                    template.voted = vote['vote']
            
        return render_to_response(
            'templates.html', {'templates': templates,
                               'current_view': 'templates',
                               'filter': f,
                               'languages': LANGUAGES,
                               'current_lang': current_lang,
                               },
            context_instance=RequestContext(request))


@login_required
def template_create(request, plusit_id=None):
    if request.method == 'POST':
        form = PlusitTemplateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('templates')
        else:
            return render(request, 'template_create.html',
                          {'form': form, 'errors': form.errors})
    else:
        form = PlusitTemplateForm()
        return render(request, 'template_create.html', {'form': form, 'current_view': 'template_create'})


@login_required
def plusit_values(request, plusit_id):
    if request.method == 'GET':
        plusit = get_object_or_404(PlusitHistorical, pk=plusit_id, user=request.user)
        values_objects = plusit.historicvalue_set.all().order_by('-value_date')
        return render_to_response(
            'plusit_edit.html', {'values_objects': values_objects,
                                 'plusit_id': plusit_id,
                                 'current_view': 'plusit_edit',
                                 'periods_choices': PERIOD_CHOICES_DICT,
                                 'stat_types': CALC_TYPES},
            context_instance=RequestContext(request))


@login_required
def plusit_value_update(request):
    if request.method == 'POST':
        name=request.POST.get('name')
        value=request.POST.get('value')
        pk=request.POST.get('pk')
        historic_value = get_object_or_404(HistoricValue, pk=int(pk), plusit__user=request.user)
        try:
            if name:
                if name == 'value_date':
                    datetime = parser.parse(value, dayfirst=True)
                    if datetime:
                        historic_value.value_date = datetime
                elif name == "value":
                    historic_value.value = int(value)
                elif name=="comment":
                    historic_value.comment = value
                historic_value.save()
            return HttpResponse(status=200)
        except:
            return HttpResponse(status=413)

        
@login_required
def trigger_public(request, plusit_id):
    plusit = get_object_or_404(PlusitHistorical, id=plusit_id, user=request.user)
    plusit.public = not plusit.public
    plusit.save()
    return HttpResponse(json.dumps({'done': True}), mimetype="application/json")


@login_required
def delete_historic_value(request, plusit_id, historic_value_id):
    get_object_or_404(HistoricValue, pk=historic_value_id, plusit__user=request.user).delete()
    plusit = get_object_or_404(PlusitHistorical, pk=plusit_id)
    values_objects = plusit.historicvalue_set.all().order_by('value_date')
    return redirect('plusit_values', plusit_id)


@login_required
def template_vote(request, template_id):
    vote_value = request.POST.get('vote')
    assert vote_value in ['up', 'down', 'neutral']
    template = PlusitTemplate.objects.get(pk=template_id)
    vote, created = TemplateVotes.objects.get_or_create(template=template, user=request.user)
    if vote.vote != VOTE_CHOICES_DICT[vote_value]:
        template.vote = template.vote+VOTE_CHOICES_DICT[vote_value]
        template.save()
    vote.vote = VOTE_CHOICES_DICT[vote_value]
    vote.save()
    return HttpResponse(json.dumps({'votes': template.vote, 'template': template.pk, 'current_vote': vote_value}), mimetype="application/json")


@login_required
def plusit_delete(request, plusit_id):
    if request.method == 'POST':
        plusit = PlusitHistorical.objects.only_normal().get(pk=plusit_id, user=request.user)
        plusit.to_deleted()
    return HttpResponse(json.dumps({'success': True}), mimetype="application/json")