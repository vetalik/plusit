from django.shortcuts import render_to_response, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout


def home(request):
    return render(request, 'home.html')

@login_required
def profile(request):
    return render_to_response('registration\profile.html')


def logout_page(request):
    return render(request, 'logout.html', {})
