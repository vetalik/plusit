import pytz
import json

from decimal import Decimal
from datetime import timedelta, datetime
from dateutil import rrule
from autoslug import AutoSlugField
from model_utils.fields import StatusField
from model_utils import Choices
from model_utils.managers import PassThroughManager

from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db.models.query import QuerySet


EMPTY = 'empt'
SUM = 'sum'
AVG = 'avg'

No_calc = 'No calculations'
Period_sum = 'Period sum'
Average = 'Average'

CALC_TYPES = {EMPTY: No_calc,
              SUM: Period_sum,
              AVG: Average,
              }
PERIOD_CHOICES_DICT = {rrule.DAILY: 'Daily',
                       rrule.WEEKLY: 'Weekly',
                       rrule.MONTHLY: 'Monthly',
                       rrule.YEARLY: 'Yearly',
                      }

CALCULATION_CHOICES = (
    (EMPTY, No_calc),
    (SUM, Period_sum),
    (AVG, Average),
)
PERIOD_CHOICES = (
    (rrule.DAILY, 'Daily'),
    (rrule.WEEKLY, 'Weekly'),
    (rrule.MONTHLY, 'Monthly'),
    (rrule.YEARLY, 'Yearly'),
)



VOTE_CHOICES = (
    (-1, 'down'),
    (0, 'neutral'),
    (1, 'up')
)
VOTE_CHOICES_DICT = {
    'down': -1, 
    'neutral':0,
    'up': 1,
}

LANGUAGES = (('eng', 'English'), ('rus', 'Russian'))


class HistoricValue(models.Model):
    value_date = models.DateTimeField(null=False)
    value = models.DecimalField(null=False, decimal_places=2,
                                max_digits=20, default=Decimal("0.0"))
    plusit = models.ForeignKey("PlusitHistorical", null=True, blank=True)
    comment = models.TextField(default='', null=True)


class PlusitHistoricalQuerySet(QuerySet):
    def only_normal(self):
        return self.filter(status=self.model.STATUS.normal)
    
    
class PlusitHistorical(models.Model):
    STATUS = Choices('normal', 'deleted')
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=1024, null=True, default="")
    user = models.ForeignKey(User)
    period_type = models.IntegerField(
        max_length=25, null=True, choices=PERIOD_CHOICES, default=rrule.DAILY)
    public = models.BooleanField(default=False, null=False)
    calculation = models.CharField(
        max_length=25, null=True, choices=CALCULATION_CHOICES, default=EMPTY)
    status = StatusField(db_index=True, default=STATUS.normal)

    objects = PassThroughManager.for_queryset_class(PlusitHistoricalQuerySet)()
    
    def to_deleted(self):
        self.status = self.STATUS.deleted
        self.save(update_fields=['status',])

    def calculate_history(self, period=None, calc_type=None):

        full_history = self.historicvalue_set.all().order_by('value_date')
        if not full_history:
            return {'history': [],
                    'axis_legend': [],
                    }
        rrule.DAILY
        all_periods = []
        dates = []
        collected_data = []
        if calc_type:
            self.calculation = calc_type
        if self.calculation == 'avg':
            method = models.Avg
        elif self.calculation == 'sum':
            method = models.Sum
        elif self.calculation == 'empt':
            for i, record in enumerate(full_history):
                all_periods.append(float(record.value))
                dates.append(record.value_date.date().isoformat())
                collected_data.append(
                    ["{0} {1}".format(record.value_date.date().isoformat(),
                                      record.value_date.time().isoformat()),
                                      float(record.value), record.comment])
            return {'history': json.dumps(all_periods),
                    'axis_legend': json.dumps(dates),
                    'stat_types': CALC_TYPES,
                    'collected_data': json.dumps([collected_data]),
                    }
        else:
            raise Exception("Not supported operation")
        first_day = full_history.aggregate(
            models.Min('value_date'))['value_date__min']
        first_day = first_day.replace(
            hour=0, minute=0, second=0, microsecond=0)
        value_param_name = 'value__{0}'.format(self.calculation)
        current_time = datetime.utcnow().replace(tzinfo=pytz.utc)
        calc_dates = list(
            rrule.rrule(period or self.period_type, dtstart=first_day, until=current_time))
        calc_dates.append(current_time)
        for i, start_day in enumerate(calc_dates):
            if i + 1 == len(calc_dates):
                continue
            end_day = calc_dates[i + 1]
            new_value = full_history.filter(value_date__gte=start_day,
                                            value_date__lt=end_day).order_by(
                'value_date').aggregate(method('value'))[value_param_name]
            if new_value:
                all_periods.append(int(new_value))
                dates.append(start_day.date().isoformat())
                collected_data.append(
                    ["{0}".format(start_day.date().isoformat()), float(new_value), ''])
        results = {'history': json.dumps(all_periods),
                   'axis_legend': json.dumps(dates),
                   'stat_types': CALC_TYPES,
                   'collected_data': json.dumps([collected_data])
                   }
        return results


class PlusitTemplate(models.Model):
    template_name = models.CharField(max_length=255)
    template_description = models.TextField()
    name = models.CharField(max_length=64, null=True)
    description = models.CharField(max_length=1024, null=True)
    period_type = models.IntegerField(
        max_length=25, null=True, choices=PERIOD_CHOICES, default=rrule.DAILY)
    calculation = models.CharField(
        max_length=25, null=True, choices=CALCULATION_CHOICES, default=EMPTY)
    vote = models.IntegerField(default=0)
    language = models.CharField(max_length=3, default='eng')
    
    def as_dict(self):
        return {
            "name": self.name,
            "description": self.description,
            "period_type": self.period_type,
            "calculation": self.calculation,
        }
    
    def get_votes(self):
        votes = TemplateVotes.objects.filter(template=self).aggregate(models.Sum('vote'))['vote__sum']
        return votes or 0


class TemplateVotes(models.Model):
    vote = models.IntegerField(max_length=1, null=True, choices=VOTE_CHOICES, default=0)
    user = models.ForeignKey(User)
    template  = models.ForeignKey('PlusitTemplate')
    def __unicode__(self):
        return "vote volume " + str(self.vote)