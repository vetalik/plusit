import settings
import traceback
import sys


class ProcessExceptionMiddleware(object):
    def process_response(self, request, response):
        if response.status_code != 200:
            print '\n'.join(traceback.format_exception(*sys.exc_info()))
        return response


# default 30 days
MAX_AGE = getattr(settings, 'CACHE_CONTROL_MAX_AGE', 2592000)

class MaxAgeMiddleware(object):
    def process_response(self, request, response):
        response['Cache-Control'] = 'max-age=%d' % MAX_AGE
        return response