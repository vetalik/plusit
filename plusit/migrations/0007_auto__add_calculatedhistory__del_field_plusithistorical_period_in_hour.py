# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CalculatedHistory'
        db.create_table('plusit_calculatedhistory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(default='0.0', max_digits=20, decimal_places=2)),
            ('plusit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['plusit.PlusitHistorical'], null=True, blank=True)),
            ('period_start', self.gf('django.db.models.fields.DateTimeField')()),
            ('period_end', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('plusit', ['CalculatedHistory'])

        # Deleting field 'PlusitHistorical.period_in_hours'
        db.delete_column('plusit_plusithistorical', 'period_in_hours')

        # Adding field 'PlusitHistorical.period_type'
        db.add_column('plusit_plusithistorical', 'period_type',
                      self.gf('django.db.models.fields.IntegerField')(default=3, max_length=25, null=True),
                      keep_default=False)


        # Changing field 'PlusitHistorical.calculation'
        db.alter_column('plusit_plusithistorical', 'calculation', self.gf('django.db.models.fields.CharField')(max_length=25, null=True))

    def backwards(self, orm):
        # Deleting model 'CalculatedHistory'
        db.delete_table('plusit_calculatedhistory')

        # Adding field 'PlusitHistorical.period_in_hours'
        db.add_column('plusit_plusithistorical', 'period_in_hours',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=1),
                      keep_default=False)

        # Deleting field 'PlusitHistorical.period_type'
        db.delete_column('plusit_plusithistorical', 'period_type')


        # Changing field 'PlusitHistorical.calculation'
        db.alter_column('plusit_plusithistorical', 'calculation', self.gf('django.db.models.fields.CharField')(max_length=25))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'plusit.calculatedhistory': {
            'Meta': {'object_name': 'CalculatedHistory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'period_end': ('django.db.models.fields.DateTimeField', [], {}),
            'period_start': ('django.db.models.fields.DateTimeField', [], {}),
            'plusit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['plusit.PlusitHistorical']", 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '20', 'decimal_places': '2'})
        },
        'plusit.historicvalue': {
            'Meta': {'object_name': 'HistoricValue'},
            'comment': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plusit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['plusit.PlusitHistorical']", 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '20', 'decimal_places': '2'}),
            'value_date': ('django.db.models.fields.DateTimeField', [], {})
        },
        'plusit.plusit': {
            'Meta': {'object_name': 'Plusit'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'plusit_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['plusit.PlusitType']"}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'value': ('django.db.models.fields.BigIntegerField', [], {'default': '0'})
        },
        'plusit.plusithistorical': {
            'Meta': {'object_name': 'PlusitHistorical'},
            'calculation': ('django.db.models.fields.CharField', [], {'default': "'empt'", 'max_length': '25', 'null': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'period_type': ('django.db.models.fields.IntegerField', [], {'default': '3', 'max_length': '25', 'null': 'True'}),
            'plusit_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['plusit.PlusitType']"}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'plusit.plusittype': {
            'Meta': {'object_name': 'PlusitType'},
            'calculable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'formula': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['plusit']