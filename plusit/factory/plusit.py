import factory
from plusit.models import Plusit

class PlusitFactory(factory.Factory):
    FACTORY_FOR = Plusit

    value = 1
    is_sum = True
    description = 'some'
    