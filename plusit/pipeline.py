from social_auth.backends.facebook import FacebookBackend
from social_auth.backends.twitter import TwitterBackend


def get_avatar_url(request, backend, response, *args, **kwargs):
    """Pipeline to get user avatar from Twitter/FB via django-social-auth"""
    avatar_url = ''
    if isinstance(backend, FacebookBackend):
        avatar_url = 'http://graph.facebook.com/%s/picture?type=large' \
                     % response['id']
    elif isinstance(backend, TwitterBackend):
        avatar_url = response.get('profile_image_url', '')
    request.session['avatar_url'] = avatar_url
    return
