import django_filters

from plusit.models import PlusitTemplate, LANGUAGES


class PlusitTemplateFilter(django_filters.FilterSet):
    language = django_filters.ChoiceFilter(choices=LANGUAGES)
    class Meta:
        model = PlusitTemplate
        fields = ['language', ]