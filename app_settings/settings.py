import os

from django.contrib import staticfiles
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS

PROJECT_PATH = os.path.abspath("..")
PROJECT_DIR = os.path.dirname(__file__) # this is not Django setting.

# Django settings for app_settings project.

DEBUG = False
TEMPLATE_DEBUG = DEBUG
ADMINS = (
     ('vetalik', 'vetalik@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ddqim1t4a1i928', # Or path to database file if using sqlite3.
        'USER': 'rslbmlaxpcntmr',                      # Not used with sqlite3.
        'PASSWORD': 'LmRbLt8rjOZpf53Qv9kQGJRX-C',                  # Not used with sqlite3.
        'HOST': 'ec2-54-243-178-223.compute-1.amazonaws.com',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}
#import dj_database_url
#DATABASES = {'default': dj_database_url.config(default='postgres://localhost')}
#print DATABASES
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = '' #os.path.join(PROJECT_DIR, 'media'),

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'static'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

AUTH_PROFILE_MODULE = "plusit.UserProfile"

# Make this unique, and don't share it with anybody.
SECRET_KEY = '^h4y*1ncjj$tts9(nyk-q_&amp;8d#5yc5*2at@zvly%2cf-l_m%si'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_backends',
    'social_auth.context_processors.social_auth_by_type_backends',
    'social_auth.context_processors.social_auth_login_redirect',
)
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cookie_message.middleware.CookieMessageMiddleware',

    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'app_settings.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'app_settings.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    "crispy_forms",
    "south",
    "plusit",
    'social_auth',
    "compressor",
    'favicon',
    'endless_pagination',
    #'google_analytics',
    'analytical',
    'cookie_message',
    'django_filters',
    'robots',
    'raven.contrib.django.raven_compat',
)

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuthBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.google.GoogleBackend',
    'social_auth.backends.yahoo.YahooBackend',
    'social_auth.backends.browserid.BrowserIDBackend',
    'social_auth.backends.contrib.linkedin.LinkedinBackend',
    'social_auth.backends.contrib.live.LiveBackend',
    'social_auth.backends.contrib.skyrock.SkyrockBackend',
    'social_auth.backends.contrib.yahoo.YahooOAuthBackend',
    'social_auth.backends.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
    )


SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    #'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'plusit.pipeline.get_avatar_url',
)
TWITTER_CONSUMER_KEY         = 'LRSLXuVI8GxfDQzDC3oH1Q'
TWITTER_CONSUMER_SECRET      = 'hjGhjOnET7V0o0AF3jKF41ESc4NXcwQWguSSE7GCnkg'
FACEBOOK_APP_ID              = '423283641074099'
FACEBOOK_API_SECRET          = '2b9ba3732f5a8eec48cedb23fbd3e0d8'
GOOGLE_OAUTH2_CLIENT_ID = '955353773798-jor7ktt9ov6j3tfh3qgofhelk3at9tbj.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'km2b9ybJLeHFsZSShYi4-FId'

LOGIN_URL          = '/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/plusits/True'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL    = '/'
SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

#try:
#    import dj_database_url
#    DATABASES['default'] =  dj_database_url.config()
#except:
#    pass


ENDLESS_PAGINATION_PER_PAGE = 5

#GOOGLE_ANALYTICS_MODEL = True
#GOOGLE_ANALYTICS_TRACK_PAGE_LOAD_TIME = True
GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-43110147-1'
GOOGLE_ANALYTICS_SITE_SPEED = True

SOCIAL_AUTH_REVOKE_TOKENS_ON_DISCONNECT = True
SOCIAL_AUTH_FORCE_POST_DISCONNECT = True

COMPRESS_ENABLED = True
ALLOWED_HOSTS = ['*']

X_FRAME_OPTIONS = 'DENY'

USERVOICE_WIDGET_KEY = 'orwGFDSIcpLBH1n29wZ8lw'

ROBOTS_CACHE_TIMEOUT = 60*60*24

COMPRESS_OFFLINE_CONTEXT = {
    'path_to_files': '/home/vetalik/plusit/static/css/',
}

TEMPLATE_VOTES_TO_HIDE=-10

RAVEN_CONFIG = {
    'dsn': 'https://aebcc2797db747519e6e820a762a8bb4:f04b034b607f4d3dbe08d420d3e289a1@app.getsentry.com/16691',
}