$(function () {

    $('#language_filter').change(function(){
        window.location=$("#language_filter option:selected").data('url')
    })
    $('.thumbs').click(function(){
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        var  vote = $(this).data('vote')
        data= {
            'csrfmiddlewaretoken': csrf,
            'vote': vote
            }
        var vote_url = $(this).data('url')
        $.post(vote_url, data,  
               function(data){
                       var vote = data['votes']
                       var template = data['template']
                       var current_vote = data['current_vote']
                       $('#votes'+template).html(vote)
                       $('#down_'+template).removeClass('voted')
                       $('#up_'+template).removeClass('voted')
                       $('#'+current_vote+'_'+template).addClass('voted')
               })
    })
})