var button_tooltip = function(el){
    var element = $(el.target)
    var tooltip = element.data('tooltip')
    console.log(tooltip)
    if (tooltip){
        element.html(tooltip)
    }
}
var button_simple = function(el){
    var element = $(el.target)
    var tooltip = element.data('simple')
    console.log(tooltip)
    if (tooltip){
        element.html(tooltip)
    }
}

$(function () {
    /*
    $('.plus-tooltip').on('mouseenter', function(el){
        button_tooltip(el)
    })
    $('.plus-tooltip').on('mouseleave', function(el){
        button_simple(el)
    })
    */
    var options = {'placement': 'top'}
    $('.plus-tooltip').tooltip(options)
});