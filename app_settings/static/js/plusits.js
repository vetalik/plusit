var draw_mini_plusit_chart = function(id, data) {
    //var chart = $('mini-chart');
    //var chart_data = $('#chart_data').data('values');
    //var axis_data = $('#axis_data').data('values');

    //$.jqplot.config.enablePlugins = true;
    options={
        legend: {show:false}  ,
        axes: {
            xaxis: {
                 showTicks: false
                
            },
            yaxis: {
                 showTicks: false
                
            },
        },
        seriesDefaults: { 
            showMarker:false
          }
        
    }
    $.jqplot(id,  data, options)
}

$(function () {

    $('.mini-chart').each(function(index){
        var chart_data = $(this).data('values')
        if(chart_data.length > 10){
            chart_data = chart_data.slice(chart_data.length-10, chart_data.length)
        }
        draw_mini_plusit_chart($(this).attr('id'), [chart_data])
    })
    var new_user = $('.new_user').data('new_user')
    if (new_user==='True'){
        var intro = introJs()
        intro.onexit(function() {
          $('.intro_icons').hide()
        })
        intro.oncomplete(function() {
          $('.intro_icons').hide()
        })
        intro.start()        
    }
    
    
});