$(function () {
    $.fn.editable.defaults.mode = 'popup';
    $('a[id^="value_date_"]').editable({
        title: 'Enter username',
        name:  'value_date',
        placement: 'right',
        datetimepicker: {
                weekStart: 1
        },
        params: function(params) {
            params.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val()
            return params;
        }

    })
    $('a[id^="comment_editable_"]').editable({
        title: 'Enter comment',
        name:  'comment',
        emptytext: '_____',
        params: function(params) {
            params.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val()
            return params;
        }
    })
    $('a[id^="value_editable_"]').editable({
        title: 'Enter comment',
        name:  'value',
        emptytext: '_____',
        params: function(params) {
            params.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val()
            return params;
        }
    })

    $('#stat_selection').change(function() {
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        data= {
            'csrfmiddlewaretoken': csrf
            }
        var default_stat_url = $('#stat_selection option:selected').data('url')
        $.post(default_stat_url, data)
    })
    $('#period_type_selection').change(function() {
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        data= {
            'csrfmiddlewaretoken': csrf
            }
        var default_period_url = $('#period_type_selection option:selected').data('url')
        $.post(default_period_url, data)
    })
    
    $("#plusit_delete").click(function(e){
        e.preventDefault()
        var url = $(this).data('url')
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        data = {'csrfmiddlewaretoken': csrf,}
        result = $.post(url, data, function( data ) {
            if (data['success']===true){
                window.location = '/plusits/'
            }
        })
    })
})
