var draw_big_plusit_chart = function() {
    var chart = $('#plus_big_chart');
    var chart_data = $('#chart_data').data('values');
    var axis_data = $('#axis_data').data('values');
    options= {axes: {
                    xaxis:{
                        renderer:$.jqplot.DateAxisRenderer,
                    }
                },
              highlighter: {
                    show: true,
                    sizeAdjust: 7.5,
                yvalues: 3,
                  formatString:'<table class="jqplot-highlighter"> <tr><td>Value: </td><td style="display: none">%s</td><td>%s</td><td>Comment: </td><td>%s</td></tr> </table>'
              },

              cursor: {
                  zoom:true,
                  looseZoom: true,
                  showTooltip:true,
                  followMouse: true,
                  showTooltipOutsideZoom: true,
                  dblClickReset: true,
                  constrainOutsideZoom: true,
              },
              rendererOptions : {type:'exponential'},
              seriesDefaults: {
                    trendline: {
                        show: true,         // show the trend line
                        color: "red",   // CSS color spec for the trend line.
                        label: "Tendency",          // label for the trend line.
                        type: "linear",     // "linear", "exponential" or "exp"
                        shadow: true,       // show the trend line shadow.
                        lineWidth: 1.5,     // width of the trend line.
                        shadowAngle: 45,    // angle of the shadow.  Clockwise from x axis.
                        shadowOffset: 1.5,  // offset from the line of the shadow.
                        shadowDepth: 3,     // Number of strokes to make when drawing shadow.
                                        // Each stroke offset by shadowOffset from the last.
                        shadowAlpha: 0.07   // Opacity of the shadow
                    }
                }

            }
    if (chart_data && chart_data[0][0]){
        $.jqplot.config.enablePlugins = true;
        var plot = $.jqplot('plus_big_chart',  chart_data, options)
        $('#reset_zoom').show()
        $('#reset_zoom').click(function(){
            plot.resetZoom()
        })
    } else{
        $('#plus_big_chart').html("<h3>Probably you don't have data yet, or selected period and calculation type produce nothing.</h3>")
    }
    
    
}

$(function () {
    $('.plus-button-vertical').click(function(){
        $('.js-form-value').submit()
    });
    $.jqplot.config.enablePlugins = true;
    draw_big_plusit_chart()
    
    $('#stat_selection').change(function() {
        window.location = $('#stat_selection option:selected').data('url')
    })
    $('#period_type_selection').change(function() {
        window.location = $('#period_type_selection option:selected').data('url')
    })
    $('.default_stat').on('click', function(e){
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        data= {
            'csrfmiddlewaretoken': csrf
            }
        var default_stat_url = $('.default_stat').data('url').replace('aaa', $('#stat_selection option:selected').val())
        $.post(default_stat_url, data)
    })
    $('.default_period').on('click', function(e){
        var csrf = $('input[name=csrfmiddlewaretoken]').val()
        data= {
            'csrfmiddlewaretoken': csrf
            }
        var default_period_url = $('.default_period').data('url').replace('aaa', $('#period_type_selection option:selected').val())
        $.post(default_period_url, data)
    })

    $('#value_date').datetimepicker({
        language: 'en'
    })
    var picker = $('#value_date').data('datetimepicker')
    picker.setDate(new Date())
    $('input[name=value_date]').on('click', function(){
        picker.show()
    })
    //$('#value_date > span').off('click')

    $('.js-calculator').zeninput({sign: true});
    $('.js-calculator').keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $("input").not("[type=submit]").jqBootstrapValidation()
});
