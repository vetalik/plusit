import os
from app_settings.settings import *


DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': './plusit.sq3', # Or path to database file if using sqlite3.
        #        'USER': '',                      # Not used with sqlite3.
        #        'PASSWORD': '',                  # Not used with sqlite3.
        #        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        #        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

FACEBOOK_APP_ID              = '110927662414413'
FACEBOOK_API_SECRET          = 'c17e76a6fec24d8769188c6d804e1a23'