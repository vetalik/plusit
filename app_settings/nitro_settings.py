import os
from app_settings.settings import *


DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': './plusit.sq3', # Or path to database file if using sqlite3.
        #        'USER': '',                      # Not used with sqlite3.
        #        'PASSWORD': '',                  # Not used with sqlite3.
        #        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        #        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

FACEBOOK_APP_ID              = '121650321338584'
FACEBOOK_API_SECRET          = '13856f221990da64962384e3f30a929f'
COMPRESS_ENABLED = True
ALLOWED_HOSTS = ['plusit.co']