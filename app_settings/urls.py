from django.conf.urls import patterns, include, url

from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.contrib.admin.sites import AdminSite


# Uncomment the next two lines to enable the admin:
from django.contrib import admin

from plusit.views import *

admin_site=AdminSite()
admin.autodiscover()

urlpatterns = patterns('',

    url('^$', plusit_home, name='home'),

    url(r'', include('social_auth.urls')),

    #(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', logout_me, name='logout'),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/$', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^tada/', include(admin.site.urls)),

    url(r'^', include('favicon.urls')),



    url(r'^plusits/$', plusits, name="plusits"),
    url(r'^plusits/(?P<new_user>\w+)/$', plusits, {'new_user': True}),
    url(r'^plusits_sq/$', plusits_sq, name="plusits_sq"),
    url(r'^plusit/(?P<plusit_id>\d+)/$', plusit, name="plusit"),
    url(r'^plusit_create/$', plusit_create, name='plusit_create'),
    url(r'^plusit_value/(?P<plusit_id>\d+)/$', plusit_value, name="plusit_value"),
    url(r'^templates/$', templates, name="templates"),
    url(r'^template_create/$', template_create, name="template_create"),
    url(r'^home/$', plusit_home, name="plusit_home"),
    url(r'^plusit_values/(?P<plusit_id>\d+)/$', plusit_values, name='plusit_values'),
    url(r'^plusit_value_update/$', plusit_value_update, name='plusit_value_update'),
    url(r'^stat_type_default/(?P<plusit_id>\d+)/(?P<stat_type>\w+)/$', stat_type_dafault, name='stat_type_dafault'),
    url(r'^period_type_default/(?P<plusit_id>\d+)/(?P<period_type>\w+)/$', period_type_default, name='period_type_default'),
    url(r'^trigger_public/(?P<plusit_id>\d+)/$', trigger_public, name="trigger_public"),
    url(r'^template_vote/(?P<template_id>\d+)/$', template_vote, name="template_vote"),
    
    url(r'^delete_historic_value/(?P<plusit_id>\d+)/(?P<historic_value_id>\d+)/$', delete_historic_value,
        name='delete_historic_value'),
    url(r'^delete_plusit/(?P<plusit_id>\d+)/$', plusit_delete, name='plusit_delete'),
    (r'^robots\.txt$', include('robots.urls')),
)
#urlpatterns += patterns(
#    '',
#    url(r'^admin/', include(admin_site.urls)),
#)
if not settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    )

urlpatterns += staticfiles_urlpatterns()

